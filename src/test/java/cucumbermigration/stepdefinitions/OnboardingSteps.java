package cucumbermigration.stepdefinitions;

import cucumbermigration.utility.DatabaseDefinitions;
import cucumbermigration.utility.FunctionLibrary;
import cucumbermigration.utility.models.*;
import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OnboardingSteps {

    private final Map<String, String> documentDefinitionIds = new HashMap<>();
    private final Map<String, String> documentFolderIds = new HashMap<>();
    private String schemaId;

    @Given("financial and onboarding folders are deleted")
    public void financialAndOnboardingFoldersAreDeleted(DataTable table) {
        List<String> foldersToDelete = table.asList();
        for (String folder : foldersToDelete) {
            FunctionLibrary.deleteTable(folder);
        }
    }

    @And("{string} FYE is set to {string}")
    public void companyFYEIsSetToMonth(String companyId, String month) {
        int monthNumber = Month.valueOf(month).getValue();
        FunctionLibrary.updateCompanySettingValue("fye_month", companyId, monthNumber);
    }

    @And("active G source is added to {string}")
    public void activeGSourceIsAddedToCompany(String companyId) {
        GRatingSource gRatingSource = new GRatingSource();
        gRatingSource.setCompanyId(companyId);
        gRatingSource.setActiveFrom("Today");
        DatabaseDefinitions.insertGRatingSource(gRatingSource);
    }

    @And("document definition is created")
    public void documentDefinitionIsCreated(DataTable table) {
        DocumentDefinition documentDefinition = new DocumentDefinition();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> cells : rows) {
            documentDefinition.setName(cells.get("name"));
            documentDefinition.setDescription(cells.get("description"));
            documentDefinition.setCategory(cells.get("category"));
            documentDefinition.setRecurrenceType(cells.get("recurrence_type"));
            documentDefinitionIds.put(cells.get("id"), DatabaseDefinitions.insertDocumentDefinition(documentDefinition));
        }
    }

    @And("document recurrence is created")
    public void documentRecurrenceIsCreated(DataTable table) {
        DocumentRecurrence documentRecurrence = new DocumentRecurrence();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> cells : rows) {
            documentRecurrence.setDocumentDefinitionId(documentDefinitionIds.get(cells.get("document_definition_id")));
            documentRecurrence.setRecurrenceInterval(cells.get("recurrence_interval"));
            documentRecurrence.setCompletedPeriodsToCreate(cells.get("completed_periods_to_create"));
            DatabaseDefinitions.insertDocumentRecurrence(documentRecurrence);
        }
    }

    @And("schema is created")
    public void schemaIsCreated(DataTable table) {
        DocumentSchema documentSchema = new DocumentSchema();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> cells : rows) {
            documentSchema.setName(cells.get("name"));
            documentSchema.setDescription(cells.get("description"));
            documentSchema.setCategory(cells.get("category"));
            schemaId = DatabaseDefinitions.insertDocumentSchema(documentSchema);
        }
    }

    @And("schema is assigned to {string}")
    public void schemaIsAssignedToCompany(String companyId) {
        DocumentSchemaCompany documentSchemaCompany = new DocumentSchemaCompany();
        documentSchemaCompany.setCompanyId(companyId);
        documentSchemaCompany.setDocumentSchemaId(schemaId);
        DatabaseDefinitions.assignDocumentSchemaToCompany(documentSchemaCompany);
    }

    @And("all document definitions are assigned to schema")
    public void allDocumentDefinitionsAreAssignedToSchema() {
        DocumentSchemaDefinition documentSchemaDefinition = new DocumentSchemaDefinition();
        documentSchemaDefinition.setDocumentSchemaId(schemaId);
        for (String DDId : documentDefinitionIds.values()) {
            documentSchemaDefinition.setDocumentDefinitionId(DDId);
            DatabaseDefinitions.assignDocumentDefinitionsToSchema(documentSchemaDefinition);
        }
    }

    @And("values are defined")
    public void valuesAreDefined() {
        //TODO
        //set date values for
        //|$CURRENT_YEAR=
        //|$CURRENT_YEAR_MINUS_1=
        //|$CURRENT_YEAR_MINUS_2=
        //|$CURRENT_YEAR_PLUS_1=
        //|$CURRENT_MONTH=
        //|$FIRST_DAY_OF_THE_CURRENT_YEAR=
    }

    @When("folders are generated for {string}")
    public void foldersAreGenerated(String companyId) {
        for (String DDId : documentDefinitionIds.values()) {
            FunctionLibrary.generateFoldersForCompany(DDId, companyId);
        }
    }

    @Then("document folders entries are created")
    public void documentFoldersCreatedEntries() {
        //TODO
        //assert that selectDocumentFolders comes back with the given data
    }

    @And("temporary table is created to keep Document folders which will be created in November-December of each year")
    public void temporaryTableIsCreated(DataTable table) {
        FunctionLibrary.copyTableFromTable("document_folder_bak", "gis2.document_folder");
        DocumentFolder documentFolder = new DocumentFolder();
        List<Map<String, String>> rows = table.asMaps(String.class, String.class);
        for (Map<String, String> cells : rows) {
            documentFolder.setDocumentDefinitionId(documentDefinitionIds.get(cells.get("document_definition_id")));
            documentFolder.setName(cells.get("name"));
            documentFolder.setCompanyId(cells.get("company_id"));
            documentFolder.setAvailableDate(cells.get("available_date"));
            documentFolder.setFolderType(cells.get("folder_type"));
            documentFolder.setDueDate(cells.get("due_date"));
            documentFolder.setFirstReminderDate(cells.get("first_reminder_date"));
            documentFolder.setSecondReminderDate(cells.get("second_reminder_date"));
            documentFolder.setStatus(cells.get("status"));
            documentFolder.setFileCreatedNotificationNameTypeId(cells.get("file_created_notification_name_type_id"));
            documentFolder.setFileApprovedNotificationNameTypeId(cells.get("file_approved_notification_name_type_id"));
            DatabaseDefinitions.insertDocumentFolder(documentFolder, true);
        }
    }

    @When("generate financials onboarding folders process is run")
    public void generateFinancialsOnboardingFoldersProcessIsRun() {
        FunctionLibrary.runAdminProcess("5969");
    }

    @Then("union of temporary table and current table should have {int} rows")
    public void unionOfTemporaryTableAndCurrentTableShouldHaveRows(int rowNumber) {
        //TODO
        //assert union of the two tables have 6 rows
    }

    @Given("date is set between {string} and {string};")
    public void processIsRunBetweenJanuaryAndOctober(String firstMonth, String lastMonth) {
        //TODO
        //make sure date is set between firstMonth and lastMonth
    }

    @And("{int} new document folder entries are created")
    public void noNewDocumentFolderEntriesAreCreated(int newDocumentFolders) {
        int allFolders = newDocumentFolders + 3;
        Assert.assertEquals(FunctionLibrary.getTableCount("gis2.document_folder"), allFolders);
    }

    @When("document definition is unassigned from schema")
    public void documentDefinitionIsUnassignedFromSchema() {
        FunctionLibrary.unassignDocumentDefinitionFromSchema(documentDefinitionIds.get("DD002"), schemaId);
        documentDefinitionIds.remove("DD002");
    }

    @Then("all document folders should be deleted")
    public void allDocumentFoldersShouldBeDeleted() {
        //TODO
        //assert that selectDocumentFolders comes back empty
    }

    @When("{string} is unassigned from schema")
    public void companyIsUnassignedFromSchema(String companyId) {
        FunctionLibrary.unassignCompanyFromSchema(companyId, schemaId);
    }

    @Then("{int} document folders should remain")
    public void documentFoldersShouldBeDeleted(int remainingDocumentFolders) {
        ;
        Assert.assertEquals(FunctionLibrary.getTableCount("gis2.document_folder"), remainingDocumentFolders);
    }
}
