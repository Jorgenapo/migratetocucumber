package cucumbermigration.utility;

import io.restassured.http.Headers;

public class FunctionLibrary {

    public static void deleteTable(String folder) {
        //TODO
        //implement execution of sql query:
        //delete from folder
    }

    public static void updateCompanySettingValue(String setting, String companyId, Integer monthNumber) {
        //TODO
        //implement execution of sql query:
        //update company set setting=!monthNumber where company_id = companyId'
    }

    public static void generateFoldersForCompany(String documentDefinitionId, String companyId) {
        Headers basicAuthHeaders = getBasicAuthorizationHeader();
        //TODO
        //implement POST request for $UAT_API_URL/generate-folders?definitionId=documentDefinitionId&companyId=companyId
    }

    public static void copyTableFromTable(String tableNew, String tableOld) {
        //TODO
        //implement execution of sql query:
        //select * into tableNew from tableOld
    }

    public static void runProcessIn(String processType, String programId) {
        Headers basicAuthHeaders = getBasicAuthorizationHeader();
        //TODO
        //implement POST request for $UAT_API_URL/run-process?programId=programId&processNameTypeId=processType
    }

    public static void runAdminProcess(String processType) {
        String programId = "Admin_Program";
        runProcessIn(processType, programId);
    }

    public static int getTableCount(String table) {
        //TODO
        //implement execution of sql query:
        //select count(*) as count from table
        return 0;
    }

    public static void unassignDocumentDefinitionFromSchema(String documentDefinitionId, String documentSchemaId) {
        //TODO
        //implement execution of sql query:
        //delete from gis2.document_schema_definition where document_definition_id = documentDefinitionId
        // and document_schema_id = documentSchemaId
    }

    public static void unassignCompanyFromSchema(String companyId, String documentSchemaId) {
        //TODO
        //implement execution of sql query:
        //delete from gis2.document_schema_company where company_id = companyId
        // and document_schema_id = documentSchemaId
    }

    private static Headers getBasicAuthorizationHeader() {
        //implement setting basic authorization header for UAT API user and password
        return new Headers();
    }

}
