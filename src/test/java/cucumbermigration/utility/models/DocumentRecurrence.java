package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentRecurrence {

    private String documentDefinitionId;
    private String recurrenceInterval;
    private String completedPeriodsToCreate;

}
