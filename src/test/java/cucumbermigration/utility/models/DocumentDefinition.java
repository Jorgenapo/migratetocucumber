package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentDefinition {

    private String name;
    private String description;
    private String category;
    private String recurrenceType;
    private int privateAccess;
    private int approvalRequired;

    public DocumentDefinition() {
        this.privateAccess = 0;
        this.approvalRequired = 0;
    }

}
