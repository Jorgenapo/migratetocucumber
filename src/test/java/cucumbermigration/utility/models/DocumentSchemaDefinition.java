package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentSchemaDefinition {

    private String documentSchemaId;
    private String documentDefinitionId;

}
