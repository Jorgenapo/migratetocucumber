package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentSchema {

    private String name;
    private String description;
    private String category;

}
