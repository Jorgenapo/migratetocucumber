package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class CompanyProgramConfig {

    private String companyId;
    private String programId;
    private String managementReportDays;
    private String auditReportDays;

}
