package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentFolder {

    private String documentDefinitionId;
    private String name;
    private String companyId;
    private String availableDate;
    private String folderType;
    private String dueDate;
    private String firstReminderDate;
    private String secondReminderDate;
    private String status;
    private String id;
    private String fileCreatedNotificationNameTypeId;
    private String fileApprovedNotificationNameTypeId;

}
