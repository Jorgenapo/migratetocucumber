package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class DocumentSchemaCompany {

    private String documentSchemaId;
    private String companyId;

}
