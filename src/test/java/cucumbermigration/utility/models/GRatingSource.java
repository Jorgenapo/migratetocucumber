package cucumbermigration.utility.models;

import lombok.Data;

@Data
public class GRatingSource {

    private String companyId;
    private String activeFrom;
    private String activeTo;
    private String programId;

}
