package cucumbermigration.utility;

import cucumbermigration.utility.models.*;

public class DatabaseDefinitions {

    public static String insertDocumentDefinition(DocumentDefinition documentDefinition) {
        //TODO
        //implement sql query and get id as String:
        //insert into gis2.document_definition (name, description, category, recurrence_type, private_access,
        //approval_required) values (documentDefinition.getName(), documentDefinition.getDescription(),
        //documentDefinition.getCategory(), documentDefinition.getRecurrenceType(), documentDefinition.getPrivateAccess(),
        //documentDefinition.getApprovalRequired());
        //select scope_identity() as id;
        return "";
    }

    public static String insertGRatingSource(GRatingSource gRatingSource) {
        //TODO
        //implement sql query and get id as String:
        //insert into etb2.g_rating_source (company_id, active_from, active_to, program_id) values
        //(gRatingSource.getCompanyId(), gRatingSource.getActiveFrom(), gRatingSource.getActiveTo(),
        //gRatingSource.getProgramId());
        //select scope_identity() as id;
        return "";
    }

    public static void insertDocumentRecurrence(DocumentRecurrence documentRecurrence) {
        //TODO
        //implement sql query:
        //insert into gis2.document_recurrence (document_definition_id, recurrence_interval, completed_periods_to_create)
        //values (%document_definition_id%, '%recurrence_interval%', ?{completed_periods_to_create=#null#});
    }

    public static String insertDocumentSchema(DocumentSchema documentSchema) {
        //TODO
        //implement sql query and get id as String:
        //insert into gis2.document_schema (name, description, category) values
        //(documentSchema.getName(), documentSchema.getDescription(), documentSchema.getCategory());
        //select scope_identity() as id;
        return "";
    }

    public static void assignDocumentSchemaToCompany(DocumentSchemaCompany documentSchemaCompany) {
        //TODO
        //implement sql query:
        //insert into gis2.document_schema_company (document_schema_id, company_id) values
        //(documentSchemaCompany.getDocumentSchemaId(), documentSchemaCompany.getCompanyId());
    }

    public static void assignDocumentDefinitionsToSchema(DocumentSchemaDefinition documentSchemaDefinition) {
        //TODO
        //implement sql query:
        //insert into gis2.document_schema_definition (document_schema_id, document_definition_id) values
        //(documentSchemaDefinition.getDocumentSchemaId(), documentSchemaDefinition.getDocumentDefinitionId());
    }

    public static DocumentFolder selectDocumentFolders() {
        //TODO
        //implement sql query and parse result into DocumentFolder class:
        //select * from gis2.document_folder
        return new DocumentFolder();
    }

    public static String insertDocumentFolder(DocumentFolder documentFolder, Boolean backup) {
        String table;
        if (backup) {
            table = "document_folder_bak";
        } else {
            table = "gis2.document_folder";
        }
        //TODO
        //implement sql query and get id as String:
        //insert into table (document_definition_id, name, company_id, available_date, folder_type,
        // due_date, first_reminder_date, second_reminder_date, status, file_created_notification_name_type_id,
        // file_approved_notification_name_type_id)
        // values
        // documentFolder parsed into values
        // select scope_identity() as id;
        return "";
    }

    public static String insertCompanyProgramConfig(CompanyProgramConfig companyProgramConfig) {
        //TODO
        //implement sql query and get id as String:
        //insert into company_program_config
        //			(company_id, program_id, management_report_days, audit_report_days)
        //                 values
        //			companyProgramConfig parsed into values
        //            select scope_identity() as id;
        return "";
    }

}
