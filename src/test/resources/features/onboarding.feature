Feature: Financials and Onboarding

  Background:
    Given financial and onboarding folders are deleted
      | etb2.g_rating_source_company                |
      | etb2.g_rating_source                        |
      | dbo.company_program_config                  |
      | gis2.program_document_definition_permission |
      | gis2.document_folder_program                |
      | gis2.document_folder_process                |
      | gis2.document_folder                        |
      | gis2.document_definition                    |
    And "Company" FYE is set to "February"
    And active G source is added to "Company"
    And document definition is created
      | name                   | description                   | category   | recurrence_type | id    |
      | Onboarding DD Yearly 1 | Descr. Onboarding DD Yearly 1 | ONBOARDING | RECURRING       | DD001 |
      | Onboarding DD Yearly 2 | Descr. Onboarding DD Yearly 2 | ONBOARDING | RECURRING       | DD002 |
      | Onboarding DD Yearly 3 | Descr. Onboarding DD Yearly 3 | ONBOARDING | RECURRING       | DD003 |
    And document recurrence is created
      | document_definition_id | recurrence_interval | completed_periods_to_create |
      | DD001                  | YEARLY              | 2                           |
      | DD002                  | YEARLY              | 1                           |
      | DD003                  | YEARLY              | 0                           |
    And schema is created
      | name                | description                | category   | id     |
      | Onboarding Schema 1 | Descr. Onboarding Schema 1 | ONBOARDING | SCH001 |
    And schema is assigned to "Company"
    And all document definitions are assigned to schema
    And values are defined
      | $CURRENT_YEAR=                  | setTo | !today (yyyy)             |
      | $CURRENT_YEAR_MINUS_1=          | setTo | !yearsFromToday (yyyy) -1 |
      | $CURRENT_YEAR_MINUS_2=          | setTo | !yearsFromToday (yyyy) -2 |
      | $CURRENT_YEAR_PLUS_1=           | setTo | !yearsFromToday (yyyy) +1 |
      | $CURRENT_MONTH=                 | setTo | !today (M)                |
      | $FIRST_DAY_OF_THE_CURRENT_YEAR= | setTo | $CURRENT_YEAR-01-01       |

  Scenario: Folder entries are created when generating folders
    When folders are generated for "Company"
    Then document folders entries are created
      | document_definition_id? | name?                 | company_id?    | available_date? | folder_type? | due_date? | first_reminder_date? | second_reminder_date? | status?        | id?     |
      | $DD001                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE | $DF001= |
      | $DD001                  | $CURRENT_YEAR_MINUS_2 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE | $DF002= |
      | $DD002                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE | $DF003= |

  Scenario: Running "Generate Financials/Onboarding folders" process before November 2nd
    Given date is set between "January" and "October";
    And folders are generated for "Company"
    And temporary table is created to keep Document folders which will be created in November-December of each year
      | document_definition_id | name                 | company_id     | available_date | folder_type | due_date | first_reminder_date | second_reminder_date | status         | file_created_notification_name_type_id | file_approved_notification_name_type_id |
      | DD001                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD002                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD003                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
    When generate financials onboarding folders process is run
    Then union of temporary table and current table should have 6 rows
      | document_definition_id? | name?                 | company_id?    | available_date? | folder_type? | due_date? | first_reminder_date? | second_reminder_date? | status?        |
      | $DD001                  | $CURRENT_YEAR_MINUS_2 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD001                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD001                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD002                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD002                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD003                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
    And 0 new document folder entries are created

  Scenario: Running "Generate Financials/Onboarding folders" process after November 2nd
    Given date is set between "November" and "December";
    And folders are generated for "Company"
    And temporary table is created to keep Document folders which will be created in November-December of each year
      | document_definition_id | name                 | company_id     | available_date | folder_type | due_date | first_reminder_date | second_reminder_date | status         | file_created_notification_name_type_id | file_approved_notification_name_type_id |
      | DD001                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD002                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD003                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
    When generate financials onboarding folders process is run
    Then union of temporary table and current table should have 6 rows
      | document_definition_id? | name?                 | company_id?    | available_date? | folder_type? | due_date? | first_reminder_date? | second_reminder_date? | status?        |
      | $DD001                  | $CURRENT_YEAR_MINUS_2 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD001                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD001                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD002                  | $CURRENT_YEAR_MINUS_1 | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD002                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
      | $DD003                  | $CURRENT_YEAR_PLUS_1  | ${COMPANY1_ID} | $TODAY          | COMPANY      | #null#    | #null#               | #null#                | NOT_APPLICABLE |
    And 3 new document folder entries are created

  Scenario: Delete some document folders before November 2nd
    Given date is set between "January" and "October";
    And folders are generated for "Company"
    And temporary table is created to keep Document folders which will be created in November-December of each year
      | document_definition_id | name                 | company_id     | available_date | folder_type | due_date | first_reminder_date | second_reminder_date | status         | file_created_notification_name_type_id | file_approved_notification_name_type_id |
      | DD001                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD002                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD003                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
    And generate financials onboarding folders process is run
    When document definition is unassigned from schema
    And folders are generated for "Company"
    Then 2 document folders should remain

  Scenario: Delete some document folders after November 2nd
    Given date is set between "November" and "December";
    And folders are generated for "Company"
    And temporary table is created to keep Document folders which will be created in November-December of each year
      | document_definition_id | name                 | company_id     | available_date | folder_type | due_date | first_reminder_date | second_reminder_date | status         | file_created_notification_name_type_id | file_approved_notification_name_type_id |
      | DD001                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD002                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD003                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
    And generate financials onboarding folders process is run
    When document definition is unassigned from schema
    And folders are generated for "Company"
    Then 4 document folders should remain

  Scenario: Delete all document folders
    Given folders are generated for "Company"
    And temporary table is created to keep Document folders which will be created in November-December of each year
      | document_definition_id | name                 | company_id     | available_date | folder_type | due_date | first_reminder_date | second_reminder_date | status         | file_created_notification_name_type_id | file_approved_notification_name_type_id |
      | DD001                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD002                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
      | DD003                  | $CURRENT_YEAR_PLUS_1 | ${COMPANY1_ID} | $TODAY         | COMPANY     | #null#   | #null#              | #null#               | NOT_APPLICABLE | #null#                                 | #null#                                  |
    And generate financials onboarding folders process is run
    When "Company" is unassigned from schema
    And folders are generated for "Company"
    Then all document folders should be deleted




